

function [err, eva_count] = my_net_eva_multiclass_dense(params, mask_data, res)

% error in terms of sequenses, not points!!!

% predict_mask=do_gen_prediction(res, mask_data);
predict_mask=do_gen_prediction_bilinear(res, mask_data);

if iscell(predict_mask)
    err_flags=false(size(mask_data));
    for s_idx=1:length(predict_mask)
        err_flags(:,:, s_idx)=predict_mask{s_idx}~=mask_data(:,:,s_idx);
    end
else
    err_flags=predict_mask~=mask_data;
end

valid_flags=mask_data~=0;
err_flags=err_flags&valid_flags;
% batch_sequence_num=size(mask_data, 3);
% err=nnz(err_flags)/numel(err_flags);
% err=nnz(err_flags)/(nnz(valid_flags)+eps);
% err=err*batch_sequence_num;

err=nnz(err_flags);
eva_count=nnz(valid_flags);
    
end


function predict_mask=do_gen_prediction(res, mask_data)

predictions = gather(res(end-1).x) ;
[~, predict_mask] = max(predictions, [], 3) ;
tmp_size=size(predict_mask);
predict_mask=reshape(predict_mask, tmp_size([1,2,4]));

mask_size=size(mask_data);
mask_size=mask_size(1:2);

predict_size=size(predict_mask);
predict_size=predict_size(1:2);

if any(mask_size~=predict_size);
    predict_mask=imresize(predict_mask, mask_size, 'nearest');
end

end


function predict_mask=do_gen_prediction_bilinear(res, mask_data)

predictions = gather(res(end-1).x) ;
predict_size=size(predictions);
predict_size=predict_size(1:2);

mask_size=size(mask_data);
mask_size=mask_size(1:2);

if all(mask_size==predict_size);
    [~, predict_mask] = max(predictions, [], 3) ;
    tmp_size=size(predict_mask);
    if length(tmp_size)<4
        tmp_size(4)=1;
    end
    predict_mask=reshape(predict_mask, tmp_size([1,2,4]));
else
    seq_num=size(predictions, 4);
    predict_mask_seqs=cell(seq_num,1);
    for s_idx=1:seq_num
        one_score_map=predictions(:,:,:, s_idx);
        one_score_map=imresize(one_score_map, mask_size);
        [~, one_predict_mask] = max(one_score_map, [], 3) ;
        predict_mask_seqs{s_idx}=one_predict_mask;
    end
    predict_mask=predict_mask_seqs;
end

end

