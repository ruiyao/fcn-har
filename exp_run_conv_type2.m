function [net, info]=exp_run_conv_type2(varargin)

addpath 'features';

input_varargin = varargin;
if ~isempty(varargin)
    opts.subject = '' ;
    opts.type = '';
    opts.task = '';
    [opts, varargin] = vl_argparse(opts, varargin) ;
else
    dir_matConvNet='../../matconvnet-1.0-beta23/matlab/';
    run([dir_matConvNet 'vl_setupnn.m']);
end

run_config=[];
init_net_path=[];

run_config.run_eva_only=false;

% when do evaluation only without training, uncomment the following:
% run_config.run_eva_only=true;
% init_net_path='../exp_run_data/OpportunityGestures_20161211054253/net-epoch-100.mat';

% run_config.run_eva_only=true;
% init_net_path='../exp_run_data/opp_all_gesture_20170103115939/net-epoch-100.mat';

init_net=[];
if ~isempty(init_net_path)
    fprintf('loading init_net from %s\n', init_net_path);
    init_net=load(init_net_path);
    init_net=init_net.net;
end

run_config.is_mean_feature = false;
% run_config.subject = 's4';
run_config.subject = 'all';
% run_config.type = 'gestures';
run_config.type = 'locomotions';
run_config.task = 'train';

% use this when do evaluation on the test set:
% run_config.ds_name='OpportunityGestures_test'; 

if ~isempty(input_varargin)
    run_config.subject = opts.subject;
    run_config.type = opts.type;
    run_config.task = opts.task;
end

run_config.overlap_param=2;

imdb=gen_ds_info(run_config);
imdb.get_batch_data_fn=@my_get_batch_data_conv_type2;

rand_str=datestr(now, 'YYYYmmDDHHMMSS');
run_id=sprintf('%s_%s', imdb.ds_name, rand_str);

% or using an existing run_id to continue training from last check point, e.g.,
% run_id='opp_all_gesture_20170103115939';

net_train_opts=[];
net_train_opts.learningRate = [0.01*ones(1, 200) 0.001*ones(1, 100)] ;
net_train_opts.numEpochs = 300;
net_train_opts.batchSize = 64;
net_train_opts.data_cache_step=2;
net_train_opts.errorFunction=@my_net_eva_multiclass_dense;
net_train_opts.errorLabels={'dense_multiclass_error'};
net_train_opts.gpus=[1];

% turn off gpu and use cpu only:
% net_train_opts.gpus=[];

if run_config.run_eva_only
    % when do evaluation only, un-comment the following:
    net_train_opts.do_net_train=false;
    net_train_opts.numEpochs=1;
    net_train_opts.data_cache_step=inf;
    
    assert(~isempty(init_net));
end

opts=[];
opts.networkType = 'simplenn' ;
[opts, varargin] = vl_argparse(opts, varargin) ;
opts.expDir = fullfile('../exp_run_data', run_id) ;
opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

if isempty(init_net)
	net = my_gen_net_conv_type2(imdb, 'networkType', opts.networkType) ;
else
	net = init_net ;
	init_net=[];
end

net.meta.classes.name = imdb.class_names;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

switch opts.networkType
  case 'simplenn', trainfn = @my_cnn_train ;
  case 'dagnn', trainfn = @my_cnn_train_dag ;
end

[net, info] = trainfn(net, imdb, 'expDir', opts.expDir, net_train_opts) ;

end
