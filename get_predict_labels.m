function [ predit_labels ] = get_predict_labels( net_train_opts, imdb )

predict_result_dir = net_train_opts.custom_opts.predict_result_dir;

if ~exist(predict_result_dir, 'dir')
    assert(false);
end

listing = dir(fullfile(predict_result_dir, '*.mat'));
score_map = zeros(size(imdb.cache_feat_data, 1), size(imdb.class_label_values, 1));
score_cnt = zeros(size(imdb.cache_feat_data, 1), 1);

for i = 1:size(listing, 1)
    batch_result = load(fullfile(predict_result_dir, listing(i).name));
    batch_result = batch_result(1).batch_result;
    batch_point_mat = batch_result.batch_info.batch_point_mat;
    void_label_flags = batch_result.batch_info.void_label_flags;
    
    for batchInd = 1:size(batch_point_mat, 2)
        testIdx = batch_point_mat(:, batchInd);
        testIdx = testIdx(void_label_flags(:, batchInd)==0);
        
        score_cnt(testIdx) = score_cnt(testIdx) + 1;
        score_map(testIdx, :) = score_map(testIdx, :) + ...
            double(batch_result.score_map{batchInd}(void_label_flags(:, batchInd)==0, :));
    end
end

score_cnt = repmat(score_cnt, 1, size(imdb.class_label_values, 1));
score_map = score_map ./ score_cnt;

[~, predict_mask] = max(score_map, [], 2) ;


predit_labels = predict_mask;
% predit_labels = [];
% for i = 1:size(imdb.cache_feat_data, 1)
%     predit_labels(i, 1) = batch_result.class_label_values(predict_mask(i));
% end

end

