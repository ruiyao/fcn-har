function [accuracy] = clsAccuracy(ActCls, Cllss)
%   inputs are ActCls = Actual Classes, Cllss = Predicted Class 
%   outpus are different accuracy measures of the classification

% f - measures
accuracy= [];
conf=confusionmat(ActCls,Cllss);
[accuracy.fmeasures accuracy.normMact accuracy.normMnull] = measures('F',conf);
accuracy.diagavg = measures('A',conf);
    
end