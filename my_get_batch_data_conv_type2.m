
% --------------------------------------------------------------------
function [batch_feat_data, batch_label_data, batch_info] = my_get_batch_data_conv_type2(imdb, batch_idxes)
% --------------------------------------------------------------------
% batch_feat_data = imdb.get_img_fn(imdb, batch) ;
% batch_label_data = imdb.get_label_fn(imdb, batch) ;

run_trn=true;
batch_val_flags=imdb.val_flags(batch_idxes);
if any(batch_val_flags)
    assert(all(batch_val_flags));
    run_trn=false;
end

assert(size(batch_idxes, 2)==1);

start_points=(batch_idxes-1).*imdb.one_sequence_length+1;

bacth_seq_num=length(batch_idxes);
val_point_start=imdb.val_point_start;

if run_trn
    % apply a random offset
    offset_values=round((rand(bacth_seq_num, 1)-0.5).*(imdb.one_sequence_length/2));
    start_points=double(start_points)+offset_values;
    start_points=max(start_points, 1);
    
    max_point_num=val_point_start-1;
else
    max_point_num=imdb.point_num;
    assert(max_point_num==size(imdb.cache_feat_data, 1));
end

gen_seq_length=imdb.one_sequence_length*imdb.overlap_param;

stop_points=start_points+gen_seq_length-1;


batch_point_mat=zeros(gen_seq_length, bacth_seq_num, 'uint32');
for s_idx=1:bacth_seq_num
    batch_point_mat(:, s_idx)=start_points(s_idx):stop_points(s_idx);
end

void_label_flags=batch_point_mat>max_point_num;

batch_point_mat=min(batch_point_mat, max_point_num);

batch_points=batch_point_mat(:);
batch_label_data=imdb.cache_label_data(batch_points,:);
batch_label_data=reshape(batch_label_data, [gen_seq_length, bacth_seq_num]);

% invalid points are not considered for training and evaluation
if any(void_label_flags)
    batch_label_data(void_label_flags)=0;
end

batch_label_data=shiftdim(batch_label_data, -1);

% don't handle label_data here, it will be handled in the dense loss layer!
% batch_label_data=reshape(batch_label_data, [1 gen_seq_length 1 bacth_seq_num]);

batch_feat_data=imdb.cache_feat_data(batch_points,:);

feat_dim=size(batch_feat_data, 2);
batch_feat_data=reshape(batch_feat_data', [feat_dim, gen_seq_length, bacth_seq_num]);

% batch_feat_data=permute(batch_feat_data, [2 1 3]);
% batch_feat_data=shiftdim(batch_feat_data, -1);

batch_feat_data=shiftdim(batch_feat_data, -1);
batch_feat_data=permute(batch_feat_data, [2 3 1 4]);

assert(size(batch_feat_data, 4)==bacth_seq_num);
assert(size(batch_feat_data, 1)==feat_dim);
assert(size(batch_feat_data, 2)==gen_seq_length);
assert(size(batch_feat_data, 3)==1);

batch_info=[];
batch_info.batch_point_mat=batch_point_mat;
batch_info.void_label_flags=void_label_flags;
    
end


