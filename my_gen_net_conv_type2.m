
% modified from cnn_mnist_init.m from MatConvnet

function net = my_gen_net_conv_type2(imdb, varargin)

opts.networkType = 'simplenn' ;
opts = vl_argparse(opts, varargin) ;

rng('default');
rng(0) ;

net = do_gen_net(imdb, opts);


% use my own dense layer:
loss_layer = struct('type', 'custom') ;
loss_layer.forward=@my_dense_softmax_forward;
loss_layer.backward=@my_dense_softmax_backward;
net.layers{end+1}=loss_layer;

net.layers=gen_padding_keep_size(net.layers);

% Fill in defaul values
net = vl_simplenn_tidy(net) ;

% Switch to DagNN if requested
switch lower(opts.networkType)
  case 'simplenn'
    % done
  case 'dagnn'
    net = dagnn.DagNN.fromSimpleNN(net, 'canonicalNames', true) ;
    net.addLayer('top1err', dagnn.Loss('loss', 'classerror'), ...
      {'prediction', 'label'}, 'error') ;
    net.addLayer('top5err', dagnn.Loss('loss', 'topkerror', ...
      'opts', {'topk', 5}), {'prediction', 'label'}, 'top5err') ;
  otherwise
    assert(false) ;
end

end


function layers=gen_padding_keep_size(layers)

for l_idx=1:length(layers)
    l=layers{l_idx};
    
    if isfield(l, 'skip_padding_fix') && l.skip_padding_fix
        continue;
    end
    
    if strcmp(l.type, 'conv')
        if iscell(l.weights)
            filter_size=size(l.weights{1});
        else
            filter_size=size(l.weights);
        end
        pad_size1=round((filter_size(1)-1)/2);
        pad_size2=round((filter_size(2)-1)/2);
        %To verify the order...
        l.pad=[pad_size1, pad_size1, pad_size2, pad_size2];
        % l.stride=[1 1];

    end
    if strcmp(l.type, 'pool')
        pool_size=l.pool;
        pad_size1=round((pool_size(1)-1)/2);
        pad_size2=round((pool_size(2)-1)/2);
        %To verify the order...

%              [TOP BOTTOM LEFT RIGHT] 
        l.pad=[pad_size1, pad_size1, pad_size2, pad_size2];

%             l.stride=[1 1];
    end
    layers{l_idx}=l;
end

end

function net = do_gen_net(imdb, opts)

input_dim=imdb.input_dim;
output_dim=imdb.class_num;

f1=1/10;
f2=1/10;

net.layers = {} ;


net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f1*randn(3, 3, 1 , 32, 'single'), zeros(1, 32, 'single')}}, ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [1 4], ...
                           'stride', 1, ...
                           'pad', 0) ;
                       

net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f1*randn(3, 3, 32, 32, 'single'),zeros(1,32,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'dilate', 1) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [1 4], ...
                           'stride', 1, ...
                           'pad', 0) ;
                    

net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f1*randn(3, 3, 32, 64, 'single'),zeros(1,64,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'dilate', 1) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [1 4], ...
                           'stride', 1, ...
                           'pad', 0) ;
  
net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f1*randn(3, 3, 32, 64, 'single'),zeros(1,64,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'dilate', 1) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [1 4], ...
                           'stride', 1, ...
                           'pad', 0) ;
        

net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f1*randn(3, 3, 32, 64, 'single'),zeros(1,64,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'dilate', 1) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [1 4], ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'dropout') ;


one_l= struct('type', 'conv', ...
                           'weights', {{f2*randn(input_dim, 1 , 64, output_dim, 'single'), zeros(1,output_dim,'single')}}, ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'dilate', 1) ;

one_l.skip_padding_fix=true;
net.layers{end+1} =one_l;
                       
end



