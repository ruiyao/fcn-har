% test S1, S2, S3, S4 and overall performance of Opportunity dataset 
% using fully convolutional network, 2d convolution
% Rui Yao, 2017-1-6

dir_matConvNet='../../matconvnet-1.0-beta23/matlab/';
run([dir_matConvNet 'vl_setupnn.m']);

subject = {'s1', 's2', 's3', 's4', 'all'};
type = {'gestures', 'locomotions'};

for i = 1:size(subject, 2)
    for j = 1:size(type, 2)
        close all;
        [net, ~] = exp_run_conv_type2('subject', subject{i}, 'type', type{j}, 'task', 'train');
        close all;
        exp_run_eva_only2('subject', subject{i}, 'type', type{j}, 'task', 'test', 'net', net);
    end
end