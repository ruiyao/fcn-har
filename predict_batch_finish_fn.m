function predict_batch_finish_fn(params, mask_data, res, batch, batch_info)

class_label_values=params.imdb.class_label_values;
[predict_mask, score_map]=do_gen_prediction_bilinear(res, mask_data);
% predict_mask=class_label_values(predict_mask);

cache_dir=params.custom_opts.predict_result_dir;
cache_file=fullfile(cache_dir, sprintf('batch_start_%d.mat', batch(1)));
batch_result=[];
batch_result.batch_info=batch_info;
batch_result.predict_mask=predict_mask;
batch_result.score_map=score_map;
batch_result.class_label_values=class_label_values;

if ~exist(cache_dir, 'dir')
    mkdir(cache_dir);
end
save(cache_file, 'batch_result');

end

function [predict_mask, score_map_seqs]=do_gen_prediction_bilinear(res, mask_data)

predictions = gather(res(end-1).x) ;

predict_size=size(predictions);
predict_size=predict_size(1:2);

mask_size=size(mask_data);
mask_size=mask_size(1:2);

seq_num=size(predictions, 4);
score_map_seqs=cell(seq_num,1);

% if all(mask_size==predict_size);
%     [~, predict_mask] = max(predictions, [], 3) ;
%     tmp_size=size(predict_mask);
%     predict_mask=reshape(predict_mask, tmp_size([1,2,4]));
% else
    
predict_mask_seqs=cell(seq_num,1);
for s_idx=1:seq_num
    one_score_map=predictions(:,:,:, s_idx);
    if ~all(mask_size==predict_size);
        one_score_map=imresize(one_score_map, mask_size);
    end
    [~, one_predict_mask] = max(one_score_map, [], 3) ;
    predict_mask_seqs{s_idx}=one_predict_mask;
    score_map_seqs{s_idx}=squeeze(one_score_map);
end
predict_mask=predict_mask_seqs;
% end


end