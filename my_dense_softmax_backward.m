
function res1=my_dense_softmax_backward(l, res1, res2)

% see file: vl_simplenn.m
% res(i) = l.backward(l, res(i), res(i+1)) ;
% res(i).dzdx = vl_nnsoftmaxloss(res(i).x, l.class, res(i+1).dzdx) ;

mask_data=do_gen_mask_data(l, res1);

res1.dzdx = vl_nnsoftmaxloss(res1.x, mask_data, res2.dzdx);

spatial_num=size(mask_data);
spatial_num=prod(spatial_num(1:2));

valid_ratio=nnz(mask_data)/numel(mask_data);
spatial_num=spatial_num.*valid_ratio+eps;

res1.dzdx=res1.dzdx./spatial_num;

end


function mask_data=do_gen_mask_data(l, res1)

mask_data=l.class;
mask_size=size(mask_data);
mask_size=mask_size(1:2);
input_size=size(res1.x);
if length(input_size)<4
    input_size(4)=1;
end
e_num=input_size(4);
input_size=input_size(1:2);

if any(mask_size~=input_size);
    mask_data=imresize(mask_data, input_size, 'nearest');
end
mask_data=reshape(mask_data, [input_size(1) input_size(2) 1 e_num]);

end

