function [net, info]=exp_run_eva_only2(varargin)

tic;

dir_matConvNet='../../matconvnet-1.0-beta23/matlab/';
run([dir_matConvNet 'vl_setupnn.m']);

addpath 'features';

input_varargin = varargin;
if ~isempty(varargin)
    opts.subject = '' ;
    opts.type = '';
    opts.task = '';
    opts.net=[];
    [opts, varargin] = vl_argparse(opts, varargin) ;
else
    dir_matConvNet='../../matconvnet-1.0-beta23/matlab/';
    run([dir_matConvNet 'vl_setupnn.m']);
end

run_config=[];
run_config.run_eva_only=true;

% init_net_path='../exp_run_data/OpportunityGestures_20161211054253/net-epoch-100.mat';

if ~isempty(input_varargin)
    init_net = opts.net;  
else
    fprintf('loading init_net from %s\n', init_net_path);
    init_net=load(init_net_path);
    init_net=init_net.net;
end

% run_config.ds_name='OpportunityGestures_test'; 
% run_config.ds_name='OpportunityLocomotion_test';

% run_config.is_mean_feature = true;
run_config.is_mean_feature = false;
% run_config.subject = 's1';
run_config.subject = 'all';
% run_config.type = 'gestures';
run_config.type = 'locomotions';
run_config.task = 'test';

if ~isempty(input_varargin)
    run_config.subject = opts.subject;
    run_config.type = opts.type;
    run_config.task = opts.task;
end

run_config.overlap_param=2;

imdb=gen_ds_info(run_config);
imdb.get_batch_data_fn=@my_get_batch_data_conv_type2;

rand_str=datestr(now, 'YYYYmmDDHHMMSS');
run_id=sprintf('%s_%s', imdb.ds_name, rand_str);

opts=[];
opts.networkType = 'simplenn' ;
[opts, varargin] = vl_argparse(opts, varargin) ;
opts.expDir = fullfile('../exp_run_data', run_id) ;
opts = vl_argparse(opts, varargin) ;

net_train_opts=[];
net_train_opts.numEpochs = 1 ;
net_train_opts.data_cache_step=inf;
net_train_opts.do_net_train=false;
net_train_opts.errorFunction=@my_net_eva_multiclass_dense;
net_train_opts.errorLabels={'dense_multiclass_error'};
net_train_opts.batch_finish_fn=@predict_batch_finish_fn;
custom_opts=[];
custom_opts.predict_result_dir=fullfile(opts.expDir, 'predict_result');
net_train_opts.custom_opts=custom_opts;

net_train_opts.batchSize = 64;

net_train_opts.gpus=[1];

% turn off gpu and use cpu only:
% net_train_opts.gpus=[];

net = init_net ;

switch opts.networkType
  case 'simplenn', trainfn = @my_cnn_train ;
  case 'dagnn', trainfn = @my_cnn_train_dag ;
end

[net, info] = trainfn(net, imdb, 'expDir', opts.expDir, net_train_opts) ;


predit_labels = get_predict_labels(net_train_opts, imdb);

err_flags = predit_labels~=imdb.cache_label_data;

valid_flags=imdb.cache_label_data~=0;
err_flags=err_flags&valid_flags;
err = nnz(err_flags)/(nnz(valid_flags)+eps);

% eval = Evaluate(imdb.cache_label_data, predit_labels)

fprintf('Percentage Correct Classification of %s for %s : %f%%\n', run_config.subject, run_config.type, 100*(1-err));

eval_raw = clsAccuracy(single(imdb.cache_label_data), single(predit_labels));
disp(['Weighted F1-score raw data with null class for ' run_config.subject ': ' num2str(eval_raw.normMnull)]);

predict_labels_dense = predit_labels - 1; % labels start by 0

% save prediction
save(['result/type2/' 'opp_' run_config.type '_' run_config.subject '_result.mat'], 'predict_labels_dense');

toc;

end




