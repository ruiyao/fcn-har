

function res2=my_dense_softmax_forward(l, res1, res2)

% see file: vl_simplenn.m
% res(i+1) = l.forward(l, res(i), res(i+1)) ;
% res(i+1).x = vl_nnsoftmaxloss(res(i).x, l.class) ;

mask_data=do_gen_mask_data(l, res1);

res2.x = vl_nnsoftmaxloss(res1.x, mask_data);

spatial_num=size(mask_data);
spatial_num=prod(spatial_num(1:2));

valid_ratio=nnz(mask_data)/numel(mask_data);
spatial_num=spatial_num.*valid_ratio+eps;

res2.x=res2.x./spatial_num;

if any(any(any(any(isnan(res2.x) | isinf(res2.x)))))
    keyboard;
end

end


function mask_data=do_gen_mask_data(l, res1)

mask_data=l.class;
mask_size=size(mask_data);
mask_size=mask_size(1:2);
input_size=size(res1.x);
if length(input_size)<4
    input_size(4)=1;
end
e_num=input_size(4);
input_size=input_size(1:2);

if any(mask_size~=input_size);
    mask_data=imresize(mask_data, input_size, 'nearest');
end
mask_data=reshape(mask_data, [input_size(1) input_size(2) 1 e_num]);


end