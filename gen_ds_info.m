
function imdb=gen_ds_info(run_config)

%some configurations:

% make the class with label 0 as the void class?
run_config.void_label_values=[];
% run_config.void_label_values=[0];

imdb=gen_imdb_type(run_config);

end


function imdb=gen_imdb_type(run_config)

void_label_values=run_config.void_label_values;
% ds_name=run_config.ds_name;

[feat_data, label_data, label_data_raw, split_info, dataMean]=load_data_type(run_config);

feat_data = bsxfun(@minus, feat_data, dataMean) ;
[class_label_values, ~, new_label_data]=unique(label_data);

if ~isempty(void_label_values)
    void_class_flags=ismember(class_label_values, void_label_values);
    void_class_idxes=find(void_class_flags);
    assert(~isempty(void_class_idxes));
    
    for v_idx=1:length(void_class_idxes)
        new_label_data(new_label_data==void_class_idxes(v_idx))=0;
    end
end

input_dim=size(feat_data, 2);
point_num=size(feat_data, 1);
class_num=length(class_label_values);
assert(point_num==numel(new_label_data))

one_sequence_length=100;
sequence_num=ceil(point_num/one_sequence_length);
assert(sequence_num>0);

val_point_start=split_info.val_point_start;
train_sequence_num=round((val_point_start-1)/one_sequence_length);

train_idxes=uint32(1:train_sequence_num)';
val_idxes=uint32(train_sequence_num+1:sequence_num)';
    
% end    

val_flags=false(sequence_num, 1);
val_flags(val_idxes)=true;

% assert(~isempty(train_idxes));
% assert(~isempty(val_idxes));

imdb=[];

% imdb.ds_name=ds_name;
imdb.ds_name = ['opp_' run_config.subject '_' run_config.type];

class_names=arrayfun(@(x)sprintf('%d',x),1:class_num,'UniformOutput',false);
imdb.class_names=class_names';
imdb.class_label_values=class_label_values;

imdb.input_dim=input_dim;
imdb.class_num=class_num;
imdb.sequence_num=sequence_num;

imdb.train_idxes=train_idxes;
imdb.val_idxes=val_idxes;
imdb.one_sequence_length=one_sequence_length;
imdb.val_flags=val_flags;

imdb.overlap_param=run_config.overlap_param;

assert(isa(feat_data, 'single'));
assert(isa(new_label_data, 'double'));

imdb.cache_feat_data=feat_data;
imdb.cache_label_data=new_label_data;
imdb.dataMean=dataMean;

imdb.point_num=point_num;
imdb.val_point_start=val_point_start;

imdb.label_data_raw = label_data_raw;

end


function [feat_data, label_data, label_data_raw, split_info, dataMean]=load_data_type(run_config)

base_path = '../../../datasets/OpportunityData/py_data_raw/';
param.method = 'mean';
param.window = 24;
param.step =12;

data_file = [base_path 'opp_' run_config.subject '_' run_config.task '_data.mat'];
label_file = [base_path 'opp_' run_config.subject '_' run_config.task '_label.mat'];

feat_data=load(data_file);
eval(sprintf('feat_data=single(feat_data.data_%s);', run_config.task));

% mean feature
if run_config.is_mean_feature
    feat_data = [zeros(size(feat_data, 1), 1) feat_data];
    feat_data = single(featureExtraction(feat_data, param, 1));
end

label_data_raw=load(label_file);
eval(sprintf('label_data_raw=label_data_raw.label_%s;', run_config.task));

% mean feature
if run_config.is_mean_feature
    for i=1:1:size(label_data_raw,2)
        label_data(:,i) = featureExtraction(label_data_raw(:,i), param, 2);
    end
else
    label_data = label_data_raw;
end

if strcmp(run_config.task, 'train')

    % using test data as validate data, rui, 2017-1-8
    data_file = [base_path 'opp_' run_config.subject '_test_data.mat'];
    label_file = [base_path 'opp_' run_config.subject '_test_label.mat'];
    
    validate_data=load(data_file);
    validate_data=single(validate_data.data_test);
    
    % mean feature
    if run_config.is_mean_feature
        validate_data = [zeros(size(validate_data, 1), 1) validate_data];
        validate_data = single(featureExtraction(validate_data, param, 1)); 
    end
    
    validate_label_raw=load(label_file);
    validate_label_raw=validate_label_raw.label_test;
    
    % mean feature
    if run_config.is_mean_feature
        for i=1:1:size(validate_label_raw,2)
            validate_label(:,i) = featureExtraction(validate_label_raw(:,i), param, 2);
        end
    else
        validate_label = validate_label_raw;
    end
    
    dataMean = mean(feat_data, 1);
        
    train_num=size(feat_data, 1);
        
    feat_data = [feat_data; validate_data];
    label_data = [label_data; validate_label];
else
    train_num=0;
        
    % calculate mean of training data
    train_file = [base_path 'opp_' run_config.subject '_train_data.mat'];
    train_data=load(train_file);
    train_data=single(train_data.data_train);
    
    % mean feature
    if run_config.is_mean_feature
        train_data = [zeros(size(train_data, 1), 1) train_data];
        train_data = featureExtraction(train_data, param, 1);
    end
    
    dataMean = mean(train_data, 1);
end

val_point_start=train_num+1;
    
if strcmp(run_config.type, 'locomotions')
    label_data = label_data(:,1);
else
    label_data = label_data(:,2);
end

split_info=[];
split_info.val_point_start=val_point_start;

assert(~isempty(feat_data));

end


